build-demos : setup demo.out linear.out plinear.out logistic.out

setup :
	if [ ! -d obj ]; then mkdir -p obj; fi

clean :
	rm -rf obj/*
	rm -f *.out

###############################################################################
logistic.out : obj/random.o obj/algebra.o obj/logistic.o
	g++ obj/random.o obj/algebra.o obj/logistic.o -o logistic.out

obj/logistic.o : src/genetic.h demos/src/algebra.h demos/src/logistic.cpp
	g++ -Isrc -Idemos/src -c demos/src/logistic.cpp -o obj/logistic.o


###############################################################################
plinear.out : obj/random.o obj/algebra.o obj/plinear.o
	g++ obj/random.o obj/algebra.o obj/plinear.o -o plinear.out

obj/plinear.o : src/genetic.h demos/src/algebra.h demos/src/plinear.cpp
	g++ -Isrc -Idemos/src -c demos/src/plinear.cpp -o obj/plinear.o


###############################################################################
linear.out : obj/random.o obj/algebra.o obj/linear.o
	g++ obj/random.o obj/algebra.o obj/linear.o -o linear.out

obj/linear.o : src/genetic.h demos/src/algebra.h demos/src/linear.cpp
	g++ -Isrc -Idemos/src -c demos/src/linear.cpp -o obj/linear.o


###############################################################################
obj/algebra.o : demos/src/algebra.cpp demos/src/algebra.h
	g++ -Isrc -Idemos/src -c demos/src/algebra.cpp -o obj/algebra.o


###############################################################################
demo.out : obj/random.o obj/demo.o
	g++ obj/random.o obj/demo.o -o demo.out

obj/demo.o : demos/src/demo.cpp obj/random.o
	g++ -Isrc -c demos/src/demo.cpp -o obj/demo.o


###############################################################################
obj/random.o : src/random.cpp src/random.h
	g++ -Isrc -c src/random.cpp -o obj/random.o
