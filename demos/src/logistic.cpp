#include "genetic.h"
#include "algebra.h"
#include <iostream>
#include <cstring>
#include <time.h>

using namespace std;


/**
 * Create GeneralizedLogistics for a genetic population. Random spawns have
 * their parameters uniformly generated over their respective ranges;
 * individuals birthed from two parents set their parameters as a normalish
 * random value between the min and max values of the corresponding parents'
 * parameter values.
 */
class LogisticSpawner : public Spawner<GeneralizedLogistic>
{
private:
    // training data to fit
    shared_ptr<DataSet> m_data;

    // bounds for each parameter
    vector<float> m_low;
    vector<float> m_high;

    const size_t NUM_PARAMS = 7;

public:
    LogisticSpawner(
        // filename where training coordinate data is stored
        const string &dataFilename,
        // lower bound for each of the seven parameters
        const vector<float> &low,
        // upper bound for each of the seven parameters
        const vector<float> &high) :

        m_data(make_shared<DataSet>(dataFilename)),
        m_low(low),
        m_high(high)
    {
        // make sure we were given bounds for each parameter
        if (m_low.size() != NUM_PARAMS || m_high.size() != NUM_PARAMS)
            throw invalid_argument("Low and high thresholds must both contain 6 values.");
    }

    virtual ~LogisticSpawner() { }

    // Create a new individual as a normalish average of two parents
    virtual IndividualPtr birth(const vector<IndividualPtr> &parents) const override
    {
        vector<float> parameters;
        parameters.reserve(NUM_PARAMS);

        for (size_t idx = 0; idx < NUM_PARAMS; idx++)
        {
            float
                minP = parents[0]->at(idx),
                maxP = parents[1]->at(idx);

            if (minP > maxP)
                swap(minP, maxP);

            parameters.push_back(normalish(minP, maxP, 3));
        }

        return make_shared<GeneralizedLogistic>(parameters, m_data);
    }

    // Create a new individual with uniformly distributed parameters
    virtual IndividualPtr random() const override
    {
        vector<float> parameters;
        parameters.reserve(NUM_PARAMS);

        for (size_t idx = 0; idx < NUM_PARAMS; idx++)
            parameters.push_back(uniform(m_low[idx], m_high[idx]));

        return make_shared<GeneralizedLogistic>(parameters, m_data);
    }
};


/**
 * Display the help message and exit
 */
void helpAndExit()
{
    cout
        << "Fit a generalized logistic function to a set of noisy points. The "
        << "generation number, score and parameters of the best solution are "
        << "printed for the first and last generations and for each generation "
        << "when a better solution is found."
        << endl;
    exit(1);
}


/**
 * Use genetic algorithms to fit a generalized logistic function to a data set
 * or display help and exit
 */
int main(int argc, char *argv[])
{
    // look for a request for help, all other arguments are ignored
    for (int idx = 0; idx < argc; idx++)
    {
        if (strcmp(argv[idx], "--help") == 0 || strcmp(argv[idx], "-h") == 0)
            helpAndExit();
    }

    // seed the random number generator
    srand(time(0));

    // this object will generate new individuals for the population
    LogisticSpawner spawner(
        // currently assuming that we're in the project root
        "demos/data/logistic.txt",
        //    A       K       B      v       Q       M       C
        {-10.0f, -10.0f, -10.0f, 0.01f, -10.0f, -10.0f, -10.0f},
        { 10.0f,  10.0f,  10.0f, 10.0f,  10.0f,  10.0f,  10.0f});

    // the container for our solutions
    Population<float, GeneralizedLogistic, LogisticSpawner>
        population(50, 2, 0.20f, 0.05f, spawner);

    runner(population, 10000);
}
