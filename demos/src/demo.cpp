#include "genetic.h"
#include <iostream>
#include <cstring>
#include <time.h>

using namespace std;


// Demo individual that measures how far a value is from another target value.
class TargetValue : public Individual<float>
{
private:
    float calculatedScore = 0.0f;

    const float &getScore() const override
    {
        return calculatedScore;
    }

public:
    const float target;
    const float value;

    TargetValue(
        // The 'answer' we are searching for
        float target,
        // The potential solution
        float value) :

        target(target),
        value(value) { }

    virtual ~TargetValue() { }

    // Calculate and return the score: the squared difference between the
    // target and value.
    virtual float score() override
    {
        calculatedScore = (target - value) * (target - value);
        return calculatedScore;
    }

    // Return the calculated score.
    virtual float score() const override
    {
        return calculatedScore;
    }

    // Write the score and value to the stream
    virtual void toStream(ostream &stream) const
    {
        stream
            << "score=" << score() << " "
            << "value=" << value;
    }
};


// Demo spawner for TargetValue individuals. Random spawns take on a uniform,
// random value; birthed spawns are the average of two parents' values plus a
// small amount of normalish noise to represent mutation.
class TargetValueSpawner : public Spawner<TargetValue>
{
public:
    const float target;
    const float rangeMin;
    const float rangeMax;
    const float mutationMax;

    TargetValueSpawner(
        // The 'answer' we are searching for
        float target,
        // The minimum value to spawn random individuals with
        float rangeMin,
        // The maximum value to spawn random individuals with
        float rangeMax,
        // The maximum amount to mutate birthed individuals with
        float mutationMax):

        target(target),
        rangeMin(rangeMin),
        rangeMax(rangeMax),
        mutationMax(mutationMax) { }

    virtual ~TargetValueSpawner() { }

    // Create a new individual from the average of two parent values with some
    // noramlish noise added to the value.
    virtual IndividualPtr birth(const vector<IndividualPtr> &parents) const override
    {
        const float x = parents[0]->value + parents[1]->value;
        const float d = mutationMax * (uniform() + uniform() + uniform() - 1.5f) / 1.5f;
        return make_shared<TargetValue>(target, x + d);

    }

    // Create a new random individual, with a value in [rangeMin, rangeMax]
    virtual IndividualPtr random() const override
    {
        const float x = uniform() * (rangeMax - rangeMin) + rangeMin;
        return make_shared<TargetValue>(target, x);
    }
};


void helpAndExit()
{
    cout
        << "'Search' for the value 0.703 in the range [-1, 1] using genetic "
        << "algorithms. The generation number, score and value of the best "
        << "solution are printed for the first and last generations and for "
        << "each generation when a better solution is found."
        << endl << endl
        << "Yes, this is a terrible way to solve this problem, but the code "
        << "at least demonstrates the usage of the core classes and how to "
        << "setup to solve a problem using genetic algorithms."
        << endl;
    exit(1);
}



int main(int argc, char *argv[])
{
    // look for a request for help, all other arguments are ignored
    for (int idx = 0; idx < argc; idx++)
    {
        if (strcmp(argv[idx], "--help") == 0 || strcmp(argv[idx], "-h") == 0)
            helpAndExit();
    }

    cout << "Searching for 0.703 in [-1, 1]" << endl;

    // seed the random number generator
    srand(time(0));

    // this object will generate new individuals for the population
    TargetValueSpawner spawner(0.703f, -1.0f, 1.0f, 0.1f);

    // the container for our 'solutions'
    Population<float, TargetValue, TargetValueSpawner>
        population(10, 2, 0.20f, 0.05f, spawner);

    runner(population, 100000);
}
