#include "genetic.h"
#include "algebra.h"
#include <iostream>
#include <cstring>
#include <time.h>

using namespace std;


/**
 * Create Lines for a genetic population. Random spawns have their parameters
 * uniformly generated over two respective ranges; individuals birthed from
 * two parents set their parameters as a normalish random value between the min
 * and max values of the corresponding parents' parameter values.
 */
class LineSpawner : public Spawner<Line>
{
private:
    shared_ptr<DataSet> m_data;
    float m_minM;
    float m_maxM;
    float m_minB;
    float m_maxB;

public:
    LineSpawner(
        // filename where training coordinate data is stored
        const string &dataFilename,
        // range for the slope parameter
        float minM, float maxM,
        // range for the intercept parameter
        float minB, float maxB) :

        m_data(make_shared<DataSet>(dataFilename)),
        m_minM(minM),
        m_maxM(maxM),
        m_minB(minB),
        m_maxB(maxB) { }

    virtual ~LineSpawner() { }

    // Create a new individual as a normalish average of two parents
    virtual IndividualPtr birth(const vector<IndividualPtr> &parents) const override
    {
        float
            minM = parents[0]->m,
            maxM = parents[1]->m,
            minB = parents[0]->b,
            maxB = parents[1]->b;

        if (minM > maxM)
            swap(minM, maxM);

        if (minB > maxB)
            swap(minB, maxB);

        const float
            m = normalish(minM, maxM, 3),
            b = normalish(minB, maxB, 3);

        return make_shared<Line>(m, b, m_data);
    }

    // Create a new individual with parameters selected from a uniform distribution
    virtual IndividualPtr random() const override
    {
        const float
            m = uniform(m_minM, m_maxM),
            b = uniform(m_minB, m_maxB);

        return make_shared<Line>(m, b, m_data);
    }
};


/**
 * Display the help message and exit
 */
void helpAndExit()
{
    cout
        << "Fit a linear function to a set of noisy points. The generation "
        << "number, score and parameters of the best solution are printed for "
        << "the first and last generations and for each generation when a "
        << "better solution is found."
        << endl << endl
        << "Yes, this could be done with linear regression, but the code at "
        << "least demonstrates the usage of the core classes and how to setup "
        << "to solve a problem using genetic algorithms."
        << endl;
    exit(1);
}


/**
 * Perform linear regression with genetic algorithms or display help and exit
 */
int main(int argc, char *argv[])
{
    // look for a request for help, all other arguments are ignored
    for (int idx = 0; idx < argc; idx++)
    {
        if (strcmp(argv[idx], "--help") == 0 || strcmp(argv[idx], "-h") == 0)
            helpAndExit();
    }

    // seed the random number generator
    srand(time(0));

    // this object will generate new individuals for the population
    LineSpawner spawner(
        // currently assuming that we're in the project root
        "demos/data/linear.txt",
        -100.0f, 100.0f,
        -100.0f, 100.0f);

    // the container for our solutions
    Population<float, Line, LineSpawner>
        population(50, 2, 0.20f, 0.05f, spawner);

    runner(population, 1000);
}
