#pragma once

#include <string>
#include "genetic.h"


/**
 * Simple container for <x,y> coordinates read from a space delimited file,
 *
 * CAUTION: This does not check line formatting, out of bounds exceptions are
 * likely if lines are not formatted exactly as expected.
 */
class DataSet
{
public:
    // A single <x,y> coordinate
    struct DataPoint
    {
        const float x;
        const float y;
        DataPoint (float x, float y) :
            x(x), y(y) { }
    };

private:
    // All of the coordinates. NOTE: since we're reading this from file once,
    // and the DataPoints are very simple, and we only provide read access to
    // the data, we aren't going to worry much about reallocation as the vector
    // grows, opting for simplicity instead of diminishing returns for speed
    std::vector<DataPoint> m_data;

    // Load coordinates from file
    void load(const std::string &filename);

public:
    // Ctor - read coordinates from the specified file
    DataSet(const std::string &filename);

    // Get a coordinate out of the list
    const DataPoint& at(size_t idx) const;

    // The number of coordinates in the list
    const size_t size() const;
};


/**
 * Base class for parameterized functions that can be evaluated in a genetic
 * algorithm framework.
 */
class ParameterizedFunction : public Individual<float>
{
private:
    // Parameters of the function
    std::vector<float> m_params;

    // Training data
    std::shared_ptr<DataSet> m_data;

    // The score of this function with respect to training data
    float m_score = 0.0f;

    const float &getScore() const override;

public:
    ParameterizedFunction(
        std::vector<float> params,
        std::shared_ptr<DataSet> data);

    virtual ~ParameterizedFunction();

    // Evaluate the function with the given parameters
    virtual float eval(float x) const = 0;

    // Compare the training data set with the function, the resulting score is
    // sum( (eval(p.x) - p.y)**2 ) for each coordinate pin the training set
    virtual float score() override;

    // Return the already-calculated score
    virtual float score() const override;

    // Return the number of parameters
    size_t size() const;

    // Return the idx'th parameter
    const float& at(size_t idx) const;

    // Write the parameter values to a stream with generic names
    virtual void toStream(std::ostream &stream) const override;
};


/**
 * A parameterized representation of the line y=mx+b
 */
class Line : public ParameterizedFunction
{
public:
    const float &m;
    const float &b;

    // Construct the line y = mx + b
    Line(float m, float b, std::shared_ptr<DataSet> data);
    virtual ~Line();

    // Evaluate the line at x
    virtual float eval(float x) const override;

    // Write named parameters to the stream
    virtual void toStream(std::ostream &stream) const override;
};


/**
 * Defines a connected, piecewise line of the form y=mx+b. The slope and y
 * intercept are calculated in a way that ensures the lines are connected at
 * their boundaries and the first and last lines are open on their lower and
 * upper bounds respectively.
 */
class PiecewiseLine : public ParameterizedFunction
{
private:
    // The last interval that the piecewise function was evaluated on. this is
    // mutable since this is only a search optimization.
    mutable size_t m_lastIdx = 0;

    // For convenience of evaluation, use Line objects to represent each piece
    std::vector<std::shared_ptr<Line>> m_f;

    // The x coordinate boundary between each piece. if there are N pieces,
    // there are N-1 boundary coordinates
    std::vector<float> m_x;

    // Check to see if x is on the idx'th interval; if it is, evaluate the
    // interval at x, return the result through an output parameter and return
    // true; otherwise, return false
    bool evalHelper(size_t idx, float x, float &y) const;

public:
    PiecewiseLine(
        // Values specifying the equations of the N lines, two parameters each:
        //  - Parameters 0 through N-1 specify the slope of each line,
        //  - Parameter N specifies the y intercept of the first line.
        //  - Parameters N+1 through 2N-1 specifies the x coordinate boundary
        //    between each line.
        // The y intercept of lines after the first are derived from the velue
        // of the previous line at the boundary and slope of the current line
        // so that the lines intersect at the boundaries
        const std::vector<float> &parameters,
        // Training data to evaluate the score of this solution
        std::shared_ptr<DataSet> data);

    virtual ~PiecewiseLine();

    // Evaluate the solution at location x
    virtual float eval(float x) const override;

    // Write named parameters to the stream
    virtual void toStream(std::ostream &stream) const override;
};

/**
 * A parameterized representation of the generalized logistic function
 * https://en.wikipedia.org/wiki/Generalised_logistic_function
 */
class GeneralizedLogistic : public ParameterizedFunction
{
public:
    GeneralizedLogistic(
        // A - lower asymptote
        // K - related to upper asymptote
        // B - growth rate
        // v>0 - controls near which asymptote max growth occurs
        // Q - related to Y(0)
        // C - related to upper asymptote
        const std::vector<float> &parameters,
        std::shared_ptr<DataSet> data);

    virtual ~GeneralizedLogistic();

    // Evaluate the function at x
    virtual float eval(float x) const override;

    // Write named parameters to the stream
    virtual void toStream(std::ostream &stream) const override;
};
