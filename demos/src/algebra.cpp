#include "algebra.h"
#include <fstream>
#include <stdexcept>
#include <math.h>

using namespace std;


/* DataSet *******************************************************************/


// Read a list of coordinates from file
DataSet::DataSet(const string &filename)
{
    load(filename);
}


// Read a list of coordinates from file
void DataSet::load(const string &filename)
{
    ifstream file(filename);
    string line;

    while (file.good())
    {
        // TODO: bounds checking, this will not be forgiving a line isn't
        // formatted exactly as expected or contains anything other than
        // two space separated floating point values
        getline(file, line);

        // parse the x-coordinate
        size_t start = line.find_first_not_of(' ');
        size_t end = line.find_first_of(' ', start + 1);
        float x = atof(line.substr(start, end - start).c_str());

        // parse the y-coordinate
        start = line.find_first_not_of(' ', end);
        end = line.size();
        float y = atof(line.substr(start, end - start).c_str());

        // store the coordinate
        m_data.emplace_back(x, y);
    }
}


// Return a coordinate from the list
const DataSet::DataPoint& DataSet::at(size_t idx) const
{
    return m_data.at(idx);
}


// Return the number of coordinates
const size_t DataSet::size() const
{
    return m_data.size();
}


/* ParameterizedFunction *****************************************************/


// Save the parameters of the function and a pointer to the training data
ParameterizedFunction::ParameterizedFunction(
    vector<float> params,
    shared_ptr<DataSet> data) :

    m_params(params),
    m_data(data) { }


// Default destructor
ParameterizedFunction::~ParameterizedFunction() { }


const float &ParameterizedFunction::getScore() const
{
    return m_score;
}


// Calculate and return the score
float ParameterizedFunction::score()
{
    m_score = 0.0f;
    for (size_t idx = 0; idx < m_data->size(); idx++)
    {
        const DataSet::DataPoint &p = m_data->at(idx);
        const float y = eval(p.x);

        // if parameters would cause the function to take on imaginary or
        // infinite values, set the score to infinity and stop
        if (isnan(y) || isinf(y))
        {
            m_score = numeric_limits<float>::infinity();
            break;
        }

        // otherwise, add the square of the error to the score
        const float dy = y - p.y;
        m_score += dy * dy;
    }

    return m_score;
}


// Return the already-calculated score
float ParameterizedFunction::score() const
{
    return m_score;
}


// Return the number of parameters
size_t ParameterizedFunction::size() const
{
    return m_params.size();
}


// Get the idx'th parameter
const float& ParameterizedFunction::at(size_t idx) const
{
    return m_params[idx];
}


// Write the named Line parameters to stream
void ParameterizedFunction::toStream(ostream &stream) const
{
    stream << "score=" << m_score;
    for (size_t idx = 0; idx < size(); idx++)
        stream << " p" << idx << "=" << at(idx);
}


/* Line **********************************************************************/


// Construct the line
Line::Line(float m, float b, std::shared_ptr<DataSet> data)
    : ParameterizedFunction({m, b}, data), m(at(0)), b(at(1)) { }


// Default destructor
Line::~Line() { }


// Return mx+b
float Line::eval(float x) const
{
    return m * x + b;
}


// Write the named parameters to stream
void Line::toStream(ostream &stream) const
{
    stream
        << "score=" << score() << " "
        << "m=" << at(0) << " "
        << "b=" << at(1);
}


/* PiecewiseLine *************************************************************/


// Construct the PiecewiseLine from parameter values
PiecewiseLine::PiecewiseLine(
    const vector<float> &parameters,
    shared_ptr<DataSet> data) :

    ParameterizedFunction(parameters, data)
{
    // we need two parameters per line
    if (size() == 0 || size() % 2 != 0)
        throw invalid_argument("PiecewiseLine requires a positive and even number of parameters");

    const size_t numPieces = size() / 2;
    m_f.reserve(numPieces);
    m_x.reserve(numPieces - 1);

    m_f.push_back(make_shared<Line>( at(0), at(numPieces), nullptr));

    // calculate the y intercept for other lines from the x coordinate
    // boundary, the slope, and the previous line value at the boundary
    for (size_t idx = 1; idx < numPieces; idx++)
    {
        const float
            m = at(idx),
            x = at(idx + numPieces),
            b = m_f.back()->eval(x) - m * x;


        m_f.push_back(make_shared<Line>(m, b, nullptr));
        m_x.push_back(x);
    }
}


// Default destructor
PiecewiseLine::~PiecewiseLine() { }


// Helper for eval()
bool PiecewiseLine::evalHelper(size_t idx, float x, float &y) const
{
    // check the first interval, which has an open lower bound
    if (idx == 0)
    {
        if (x < m_x[idx])
        {
            y = m_f[idx]->eval(x);
            return true;
        }
    }
    // check the last interval, which has an open upper bound
    else if (idx == m_f.size() - 1)
    {
        if(x >= m_x[idx - 1])
        {
            y = m_f[idx]->eval(x);
            return true;
        }
    }
    // check a middle interval with set lower and upper bounds
    else
    {
        if (x >= m_x[idx - 1] && x < m_x[idx])
        {
            y = m_f[idx]->eval(x);
            return true;
        }
    }

    // x isn't on the specified interval
    return false;
}


// Calculate the value at x
float PiecewiseLine::eval(float x) const
{
    // handle the trivial case when there is only one piece
    if (m_f.size() == 1)
        return m_f[0]->eval(x);

    float y = 0.0f;

    // check to see if this x is in the same interval as the last one
    if (evalHelper(m_lastIdx, x, y))
        return y;

    // x is in some other interval, find it
    for (size_t idx = 0; idx < m_f.size(); idx++)
    {
        // assuming the data is in sorted order and the intervals aren't
        // smaller than the space between data, the next interval should
        // be the one we want, so start looking there. otherwise, continue
        // through the list and wrap around if necessary.
        size_t pieceIdx = (idx + m_lastIdx + 1) % m_f.size();
        if (evalHelper(pieceIdx, x, y))
        {
            // NOTE: m_lastIdx is mutable
            m_lastIdx = pieceIdx;
            return y;
        }
    }

    // this should not happen, any x should be in some range
    throw out_of_range("Impossible x outside of PiecewiseLine boundaries");
}


// Write the named parameters to stream
void PiecewiseLine::toStream(ostream &stream) const
{
    stream << "score=" << score();
    for (size_t idx = 0; idx < size(); idx += 2)
    {
        stream
            << " m" << idx << "=" << at(idx)
            << (idx == 0 ? " b" : " x") << idx << "=" << at(idx + 1);
    }
}


/* GeneralizedLogistic *******************************************************/


// Construct the GeneralizedLogistic function from parameters
GeneralizedLogistic::GeneralizedLogistic(
    const vector<float> &parameters,
    shared_ptr<DataSet> data) :

    ParameterizedFunction(parameters, data) { }


// Default destructor
GeneralizedLogistic::~GeneralizedLogistic() { }


// Evaluate the logistic function at x
float GeneralizedLogistic::eval(float x) const
{
    // shorthand to match formula on wikipeia
    const float
        &A = at(0),
        &K = at(1),
        &B = at(2),
        &v = at(3),
        &Q = at(4),
        &M = at(5),
        &C = at(6);

    // the function is only defined for v>0
    if (v <= 0.0f)
        return nan("");

    // for the function to be real valued, a negative value cannot be raised
    // to a fractional power
    const float base = C + Q * exp(-B * (x - M));
    if (base < 0.0f)
        return nan("");

    return A + (K - A) / pow(base, 1 / v);
}


// Write the named parameters to stream
void GeneralizedLogistic::toStream(ostream &stream) const
{
    stream
        << "score=" << score() << " "
        << "A=" << at(0) << " "
        << "K=" << at(1) << " "
        << "B=" << at(2) << " "
        << "v=" << at(3) << " "
        << "Q=" << at(4) << " "
        << "M=" << at(5) << " "
        << "C=" << at(6);
}
