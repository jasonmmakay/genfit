#include "random.h"


// Generate a float in [0, 1)
float frand()
{
    // rand() is in the range [0, RAND_MAX)
    return ((float)rand() / RAND_MAX);
}


// Generate a float in [0, 1]
float uniform()
{
    return ((float)rand() / (RAND_MAX - 1));
}


// Generate a float in [low, high]
float uniform(float low, float high)
{
    return uniform() * (high - low) + low;
}


// Generate an approximately normal float in [low, high]
float normalish(float low, float high, size_t samples)
{
    float x = 0.0f;
    for (size_t count = 0; count < samples; count++)
        x += uniform();

    return (x / samples) * (high - low) + low;
}


// Generate an int in [0, size-1]
size_t randRange(size_t size)
{
    size_t result = (size_t)(size * frand());

    // probably paranoia, but protect against rounding
    if (result == size && size != 0)
        result--;

    return result;
}
// Get a part of the whole number, with noise for the fractional part
size_t randPart(float proportion, size_t total)
{
    if (proportion <= 0.0f)
        return 0;
    else if (proportion >= 1.0f)
        return 1;

    size_t wholePart = (size_t)(total * proportion);
    float fractionPart = (float)total * proportion - (float)wholePart;

    if (uniform() < fractionPart)
        wholePart += 1;

    return wholePart;
}


/* IndexShuffleCycler *******************************************************/

// Initialize the indices
IndexShuffleCycler::IndexShuffleCycler(size_t count)
{
    m_indices.reserve(count);
    while (m_indices.size() < count)
    {
        size_t idx = m_indices.size();
        m_indices.push_back(idx);
    }

    shuffle(m_indices);
}


// Generate the next index
size_t IndexShuffleCycler::next()
{
    if (m_index >= m_indices.size())
    {
        shuffle(m_indices);
        m_index = 0;
    }

    return m_indices[m_index++];
}