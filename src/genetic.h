#pragma once

#include "random.h"

#include <vector>
#include <memory>
#include <algorithm>
#include <iostream>


/**
 * Base class for an individual in a population. Though most problems may
 * require more, the only responsibility an individual has for the genetic
 * algorithm is to calculate its fitness score, which determines its ranking
 * in a population.
*/
template <typename ScoreT>
class Individual
{
private:
    // Internal score reference
    virtual const ScoreT &getScore() const = 0;

public:
    virtual ~Individual() { }

    // Calculate the fitness score. This version of the method is called once
    // when the individual is created.
    virtual ScoreT score() = 0;

    // Return the pre-calculated fitness score. This version of the method is
    // called whenever the population must be sorted.
    virtual ScoreT score() const = 0;

    // Write a string representation to a stream.
    virtual void toStream(std::ostream &stream) const = 0;

    // Compare the scores of two individuals
    friend bool operator<(const Individual &a, const Individual &b)
    {
        return a.getScore() < b.getScore();
    }
};



// Stream output operator for all Individual types, calls toStream() on the
// individual object.
template <typename ScoreT>
std::ostream& operator << (
    std::ostream &stream,
    const Individual<ScoreT> &obj)
{
    obj.toStream(stream);
    return stream;
}


/**
 * Base class for generating new individuals for a population, by either
 * creating a completely random individual without regard to the rest of the
 * population, or by combining the attributes of parents from the population.
*/
template <typename IndividualT>
class Spawner
{
protected:
    // Protected shorthand typedef for access in derived classes
    using IndividualPtr = std::shared_ptr<IndividualT>;

public:
    virtual ~Spawner() { }

    // Create a new individual by combining attributes from parents.
    virtual IndividualPtr birth(const std::vector<IndividualPtr> &parents) const = 0;

    // Create a random individual without regard for the current population.
    virtual IndividualPtr random() const = 0;
};



/**
 * An implementation of the genetic algorithm. This collection generates new
 * individuals via random processes each generation (iteration), displacing
 * those currently in the population that have the worst scores. The best
 * scoring element is maintained in the front of the collection.
 *
 * The template parameters specify the class of the individuals and the class
 * used to spawn them and must be related to each other such that IndividualT
 * implements the base class Individual, and SpawnerT implements the base
 * class Spawner<IndividualT>, ie, it must spawn the same type of individuals.
*/
template <typename ScoreT, typename IndividualT, typename SpawnerT>
class Population
{
private:
    // Inheritence checking of template types
    static_assert(
        std::is_base_of<Individual<ScoreT>, IndividualT>::value,
        "Individual template parameter must derive from class Individual<ScoreT>");
    static_assert(
        std::is_base_of<Spawner<IndividualT>, SpawnerT>::value,
        "Spawner template parameters must derive from class Spawner<IndividualT>");

    // Shorthand typedefs
    using IndividualPtr = std::shared_ptr<IndividualT>;
    using PopList = std::vector<IndividualPtr>;

    // See constructor arguments for descriptions
    const size_t m_targetSize;
    const size_t m_parentCount;
    const float m_birthRate;
    const float m_randomRate;
    SpawnerT m_spawner;
    // End constructor arguments

    // The current population
    PopList m_population;

    // Sequential counter for the number of generations that have passed
    size_t m_generation = 0;

    // Sort individuals in the population by score
    static bool comparator(const IndividualPtr &a, const IndividualPtr &b)
    {
        return a->score() < b->score();
    }


public:
    // No default constructor
    Population() = delete;

    // Create the empty population
    Population(
        // The target size of the population; if the birth and random spawn
        // rate sum to greater than 1, them there may be more individuals in
        // the population than this threshold
        size_t targetSize,
        // Number of individuals needed to spawn a new one
        size_t parentCount,
        // The number of new births per current population member
        float birthRate,
        // The rate of new random spawns per current population member
        float randomRate,
        // Object to birth and spawn new individuals
        const SpawnerT &spawner) :

        m_targetSize(targetSize),
        m_parentCount(parentCount),
        m_birthRate(birthRate),
        m_randomRate(randomRate),
        m_spawner(spawner) { }

    // Create the next generation, removing the least fit individuals from the
    // population to make room for the new, and order the population based on
    // the individuals fitness scores so that the most fit is always at the
    // front of the collection
    void next()
    {
        const size_t currentCount = m_population.size();

        // initialize under the assumption that the population is too
        // small to have any births and only random spawns will occur
        size_t birthCount = 0;
        size_t randomCount = m_parentCount;

        // check to see if we can support any births
        if (currentCount >= m_parentCount)
        {
            birthCount = randPart(m_birthRate, currentCount);
            randomCount = randPart(m_randomRate, currentCount);
        }

        // allocate some space for new individuals
        PopList newPopulation;
        newPopulation.reserve(birthCount + randomCount);

        // randomly generate indices into the current population in such a way
        // that any parent can be selected at most once more than any other
        IndexShuffleCycler parentIndices(currentCount);

        // initialize with nulls, we will overwrite the pointers at the correct
        // slots when selecting parents
        PopList parents(m_parentCount, nullptr);

        // create new individuals via birth from parents
        for (size_t cnt = 0; cnt < birthCount; cnt++)
        {
            // choose parents
            for (size_t idx = 0; idx < m_parentCount; idx++)
                parents[idx] = m_population.at(parentIndices.next());

            // birth a new individual and score it
            newPopulation.push_back(m_spawner.birth(parents));
            newPopulation.back()->score();
        }

        // create new individuals via random spawing
        for (size_t cnt = 0; cnt < randomCount; cnt++)
        {
            newPopulation.push_back(m_spawner.random());
            newPopulation.back()->score();
        }

        // now kill enough individuals to make room for the new ones
        size_t liveOnCount = currentCount;

        // more individuals were created than we can support, kill the entire
        // current population to make way for the new
        if (birthCount + randomCount > m_targetSize)
            liveOnCount = 0;
        // otherwise, figure out how much space there is for old individuals
        // to remain after adding the new ones
        else
            liveOnCount = m_targetSize - (birthCount + randomCount);


        // if room needs to be made for the newcomers, leave the most fit from
        // the front of the list in the population, remove the rest
        if (liveOnCount < currentCount)
            m_population.erase(
                m_population.begin() + liveOnCount,
                m_population.end());

        // add the new members
        m_population.insert(
            m_population.end(),
            newPopulation.begin(),
            newPopulation.end());

        // sort the entire population based on score
        std::sort(
            m_population.begin(),
            m_population.end(),
            comparator);

        // one generation has now passed
        m_generation++;
    }

    // Return the nth-ranked individual
    const IndividualPtr at(size_t idx) const
    {
        return m_population.at(idx);
    }

    // Return the individual with the best score
    const IndividualPtr front() const
    {
        return m_population.front();
    }

    // Return the individual with the worst score
    const IndividualPtr back() const
    {
        return m_population.back();
    }

    // Return the number of individuals in the population
    size_t size() const
    {
        return m_population.size();
    }

    // Return the number of generations that have passed
    size_t generation() const
    {
        return m_generation;
    }
};


/**
 * A basic driver to run a population for a number of generations and print the
 * generation number and best individual from the population on the first and
 * last generation and whenever a better individual is found.
 */
template <typename ScoreT, typename IndividualT, typename SpawnerT>
void runner(
    Population<ScoreT, IndividualT, SpawnerT> &population,
    const size_t maxGeneration)
{
    std::shared_ptr<IndividualT> best;
    while (population.generation() < maxGeneration)
    {
        // move the population ahead a generation, creating new individuals
        population.next();

        // check to see if we need to display anything
        if (population.generation() == 1
            || population.generation() == maxGeneration
            || *population.front() < *best)
        {
            best = population.front();

            std::cout
                << "generation=" << population.generation() << " "
                << *best << std::endl;
        }
    }
}

